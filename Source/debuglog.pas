unit DebugLog;

{$mode objfpc}{$H+}

interface

procedure WriteLine(message: string);
procedure Write(message: string);

implementation

procedure WriteLine(message: string);
begin
    {$ifdef GOF_DEBUG}
         writeln(message);
    {$endif}
end;

procedure Write(message: string);
begin
    {$ifdef GOF_DEBUG}
         write(message);
    {$endif}
end;

end.
