unit GameOfLife;

{$mode ObjFPC}{$H+}

interface

uses CommonTypes, DebugLog;

type
    CellPtr = ^Cell;

    OffscreenBuffer = record
        Data: u32_ptr;
        Size: u64;
        Width: u32;
        Height: u32;
    end;

    Cell = record
        IsAlive: bool;
        CreateOrPreserve: bool;
    end;

// Forward declarations
procedure Initialialize();
procedure UpdateAndRender(const offscreenBuffer: OffscreenBuffer; const ts: f32);
procedure OnSimulationCommand(const command: SimulationCommand; const x, y: i32);

procedure UpdateSimulation();

implementation

const
    CELL_WIDTH = 30;
    CELL_HEIGHT = 30;
    GRID_OFFSET_X = 45;
    GRID_OFFSET_Y = 32;

var
    g_Cells: array of Cell;
    g_CellsWidth: i32 = 50;
    g_CellsHeight: i32 = 27;

    g_SimulationSpeed: f32 = 1.0;
    g_SimulationUpdateTimer: f32 = 0.0;
    g_SimulationPlaying: bool = False;

function GetCellPtr(const x, y: i32): CellPtr;
begin
    Result := @g_Cells[y * g_CellsWidth + x];
end;

function IsCellAlive(const x, y: i32): bool;
begin
    Result := g_Cells[y * g_CellsWidth + x].IsAlive;
end;

procedure OnSimulationCommand(const command: SimulationCommand; const x, y: i32);
var
    offsetX: i32;
    offsetY: i32;
    i, j: i32;
begin
    offsetX := x - GRID_OFFSET_X;
    offsetY := y - GRID_OFFSET_Y;

    case command of
        ResumeOrPauseSimulation:
        begin
            g_SimulationPlaying := g_SimulationPlaying = False;
            g_SimulationUpdateTimer := 0.0;
            //UpdateSimulation();
            DebugLog.WriteLine('Changed simulation state');
        end;
        SpeedUpSimulation:
        begin
            g_SimulationSpeed := Min(g_SimulationSpeed + 0.1, 3.0);
            DebugLog.WriteLine('Speed up the simulation');
        end;
        SlowDownSimulation:
        begin
            g_SimulationSpeed := Max(g_SimulationSpeed - 0.1, 1.0);
            DebugLog.WriteLine('Slow down the simulation');
        end;
        KillOrReviveCell:
        begin
            DebugLog.WriteLine('Mouse button pressed');
            for i := 0 to g_CellsHeight - 1 do
            begin
                for j := 0 to g_CellsWidth - 1 do
                begin
                    if ((offsetX > j * CELL_WIDTH) and (offsetX < (j + 1) * CELL_WIDTH) and (offsetY > i * CELL_HEIGHT) and (offsetY < (i + 1) * CELL_HEIGHT)) then
                    begin
                        (GetCellPtr(j, i)^).IsAlive := (GetCellPtr(j, i)^).IsAlive = False;
                    end;
                end;
            end;
        end;
    end;
end;

procedure Initialialize();
var
    i: u32;
begin
    SetLength(g_Cells, g_CellsWidth * g_CellsHeight);

    for i := 0 to g_CellsWidth * g_CellsHeight - 1 do
    begin
        g_Cells[i].IsAlive := False;
        g_Cells[i].CreateOrPreserve := False;
    end;

    // Create ship
    (GetCellPtr(3, 3)^).IsAlive := True;
    (GetCellPtr(4, 3)^).IsAlive := True;
    (GetCellPtr(5, 3)^).IsAlive := True;
    (GetCellPtr(5, 2)^).IsAlive := True;
    (GetCellPtr(4, 1)^).IsAlive := True;
end;

procedure DrawRectangle(const offscreenBuffer: OffscreenBuffer; const _minX, _minY, _maxX, _maxY: i32; const color: u32);
var
    x, y: i32;
    minX, minY, maxX, maxY: i32;
begin
    minX := Max(0, _minX);
    minY := Max(0, _minY);
    maxX := Min(offscreenBuffer.Width, _maxX);
    maxY := Min(offscreenBuffer.Height, _maxY);

    for y := minY to maxY - 1 do
    begin
        for x := minX to maxX - 1 do
        begin
            offscreenBuffer.Data[y * offscreenBuffer.Width + x] := color;
        end;
    end;
end;

procedure DrawBackground(const offscreenBuffer: OffscreenBuffer; const color: u32);
var
    i: u32;
begin
    for i := 0 to offscreenBuffer.Size - 1 do
    begin
        offscreenBuffer.Data[i] := color;
    end;
end;

function CountAliveNeighbours(x, y: i32): i32;
var
    i, j, nX, nY: i32;
begin
    Result := 0;
    for i := -1 to 1 do
    begin
        for j := -1 to 1 do
        begin
            if (i = 0) and (j = 0) then
                Continue;
            nX := (x + i + g_CellsWidth) mod g_CellsWidth;
            nY := (y + j + g_CellsHeight) mod g_CellsHeight;

            if IsCellAlive(nX, nY) then
                Result := Result + 1;
        end;
    end;
end;

procedure UpdateSimulation();
var
    i, x, y: i32;
    currentCell: CellPtr;
    neighboursAlive: u32;
begin
    DebugLog.WriteLine('Simulation updated');

    // Prepare changes
    for y := 0 to g_CellsHeight - 1 do
    begin
        for x := 0 to g_CellsWidth - 1 do
        begin
            currentCell := GetCellPtr(x, y);

            // Count alive neighbours
            neighboursAlive := CountAliveNeighbours(x, y);

            // Rules
            if (currentCell^).IsAlive then
                (currentCell^).CreateOrPreserve := (neighboursAlive >= 2) and (neighboursAlive <= 3)
            else
                (currentCell^).CreateOrPreserve := (neighboursAlive = 3);
        end;
    end;

    // Commit changes
    for i := 0 to g_CellsWidth * g_CellsHeight - 1 do
    begin
        g_Cells[i].IsAlive := g_Cells[i].CreateOrPreserve;
    end;
end;

procedure UpdateAndRender(const offscreenBuffer: OffscreenBuffer; const ts: f32);
var
    x, y: i32;
    color: u32;
begin

    // Simulate
    if (g_SimulationPlaying) then
    begin
        color := $ff00ff00;
        if (g_SimulationUpdateTimer > 1) then
        begin
            g_SimulationUpdateTimer := 0.0;
            UpdateSimulation();
        end;
        g_SimulationUpdateTimer := (g_SimulationUpdateTimer + ts) * g_SimulationSpeed;
    end
    else
    begin
        color := $ffff0000;
    end;

    // Render

    // Draw background first
    DrawBackground(offscreenBuffer, $ff555555);

    // Render cells
    for y := 0 to g_CellsHeight - 1 do
    begin
        for x := 0 to g_CellsWidth - 1 do
        begin
            DrawRectangle(offscreenBuffer, (GRID_OFFSET_X + x * CELL_WIDTH), (GRID_OFFSET_Y + y * CELL_HEIGHT), (GRID_OFFSET_X + (x + 1) * CELL_WIDTH), (GRID_OFFSET_Y + (y + 1) * CELL_HEIGHT), $ff555555);
            if (IsCellAlive(x, y)) then
            begin
                DrawRectangle(offscreenBuffer, (GRID_OFFSET_X + x * CELL_WIDTH) + 1, (GRID_OFFSET_Y + y * CELL_HEIGHT) + 1, (GRID_OFFSET_X + (x + 1) * CELL_WIDTH) - 1, (GRID_OFFSET_Y + (y + 1) * CELL_HEIGHT) - 1, color);
            end
            else
            begin
                DrawRectangle(offscreenBuffer, (GRID_OFFSET_X + x * CELL_WIDTH) + 1, (GRID_OFFSET_Y + y * CELL_HEIGHT) + 1, (GRID_OFFSET_X + (x + 1) * CELL_WIDTH) - 1, (GRID_OFFSET_Y + (y + 1) * CELL_HEIGHT) - 1, $ff333333);
            end;
        end;
    end;
end;

end.
