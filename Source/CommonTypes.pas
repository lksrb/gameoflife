unit CommonTypes;

{$mode ObjFPC}{$H+}

interface

// Primitive types
type
    i32 = integer;
    i64 = int64;
    u32 = cardinal;
    u64 = uint64;
    bool = boolean;
    f32 = real;

    // Pointer types
    u32_ptr = ^u32;

    //  Commands enum type
    SimulationCommand = (ResumeOrPauseSimulation, SpeedUpSimulation, SlowDownSimulation, KillOrReviveCell);

function Min(a, b: f32): f32;
function Max(a, b: f32): f32;
function Min(a, b: i32): i32;
function Max(a, b: i32): i32;

implementation

function Min(a, b: f32): f32;
begin
    if a < b then
        Result := a
    else
        Result := b;
end;

function Max(a, b: f32): f32;
begin
    if a > b then
        Result := a
    else
        Result := b;
end;

function Min(a, b: i32): i32;
begin
    if a < b then
        Result := a
    else
        Result := b;
end;

function Max(a, b: i32): i32;
begin
    if a > b then
        Result := a
    else
        Result := b;
end;

end.
