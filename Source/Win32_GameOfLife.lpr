program Win32_GameOfLife;

{$mode objfpc}{$H+}

uses
    CommonTypes,
    Windows,
    GameOfLife,
    DebugLog;

type
    // Platform types
    Win32_Window = record
        Instance: Windows.HINST;
        WindowClass: WNDCLASS;
        Handle: Windows.HWND;
        Width, Height: i32;
    end;

    Win32_OffscreenBuffer = record
        Bitmap: Pointer;
        BitmapSize: u64;
        BitmapInfo: Windows.BITMAPINFO;
    end;

    Win32_Platform = record
        Frequency: u64;
        Offset: u64;
    end;

    // Global variables
var
    g_Platform: Win32_Platform;
    g_Window: Win32_Window;
    g_OffscreenBuffer: Win32_OffscreenBuffer;
    g_IsRunning: bool = True;

    // Procedures & functions
    function GetTimeStep(): f32;
    var
        timer: u64;
    begin
        QueryPerformanceCounter(@timer);
        Result := (timer - g_Platform.Offset) / g_Platform.Frequency;
    end;

    procedure ResizeOffscreenBuffer(const Width, Height: i32);
    begin
        // Window is minimized
        if (Width = 0) or (Height = 0) then
            Exit;

        if ((g_Window.Width = Width) and (g_Window.Height = Height)) then
            Exit;

        // Update window dimensions
        g_Window.Width := Width;
        g_Window.Height := Height;

        if g_OffscreenBuffer.Bitmap <> nil then
        begin
            VirtualFree(g_OffscreenBuffer.Bitmap, 0, MEM_RELEASE);
        end;

        g_OffscreenBuffer.BitmapInfo.bmiHeader.biSize := sizeOf(bitmapInfo.bmiHeader);
        g_OffscreenBuffer.BitmapInfo.bmiHeader.biWidth := Width;
        g_OffscreenBuffer.BitmapInfo.bmiHeader.biHeight := -Height; // top-left
        g_OffscreenBuffer.BitmapInfo.bmiHeader.biPlanes := 1;
        g_OffscreenBuffer.BitmapInfo.bmiHeader.biBitCount := 32; // R8G8B8 and padding
        g_OffscreenBuffer.BitmapInfo.bmiHeader.biCompression := BI_RGB; // No comression

        // Allocate backbuffer
        g_OffscreenBuffer.BitmapSize := Width * Height * 4;
        g_OffscreenBuffer.Bitmap := VirtualAlloc(nil, g_OffscreenBuffer.BitmapSize, MEM_COMMIT, PAGE_READWRITE);

        {$ifdef GOF_DEBUG}
        	writeln('Resized backbuffer: ', Width, ', ', Height);
        {$endif}
    end;

    procedure DisplayOffscreenBuffer(deviceContext: HDC; const x, y, Width, Height: i32);
    begin
        // Rect to rect copy
        StretchDIBits(deviceContext, 0, 0, Width, Height, 0, 0, g_Window.Width, g_Window.Height, g_OffscreenBuffer.Bitmap, g_OffscreenBuffer.BitmapInfo, DIB_RGB_COLORS, SRCCOPY);
    end;

    function WindowProc(hWnd: Windows.HWND; Msg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
    var
        paint: PAINTSTRUCT;
        deviceContext: HDC;
        x, y, Width, Height: i32;
        clientArea: RECT;
        keyCode: WPARAM;
        wasKeyDown: bool;
        isKeyDown: bool;
    begin
        Result := 0;
        case Msg of
            WM_SIZE:
            begin
                GetClientRect(g_Window.Handle, clientArea);
                Width := clientArea.right - clientArea.left;
                Height := clientArea.bottom - clientArea.top;
                ResizeOffscreenBuffer(Width, Height);
                Result := 0;
            end;

            WM_DESTROY:
            begin
                PostQuitMessage(0);
                Result := 0;
            end;
            WM_CLOSE:
            begin
                g_IsRunning := False;
                Result := 0;
            end;

            WM_PAINT:
            begin
                deviceContext := BeginPaint(g_Window.Handle, paint);
                x := paint.rcPaint.left;
                y := paint.rcPaint.top;
                Width := paint.rcPaint.right - paint.rcPaint.left;
                Height := paint.rcPaint.bottom - paint.rcPaint.top;
                DisplayOffscreenBuffer(deviceContext, x, y, Width, Height);
                //PatBlt(deviceContext, x, y, width, height, WHITENESS);
                EndPaint(g_Window.Handle, &paint);

                Result := 0;
            end;

            WM_LBUTTONDOWN, WM_RBUTTONDOWN:
            begin
                x := GET_X_LPARAM(lParam);
                y := GET_Y_LPARAM(lParam);
                GameOfLife.OnSimulationCommand(KillOrReviveCell, x, y);
            end;
            WM_SYSKEYDOWN, WM_KEYDOWN:
            begin
                keyCode := wParam;
                wasKeyDown := (lParam and (1 shl 30)) <> 0;
                isKeyDown := (lParam and (1 shl 31)) = 0;

                // Don't allow repeats
                if (isKeyDown <> wasKeyDown) then
                begin
                    case keyCode of
                        VK_SPACE:
                        begin
                            GameOfLife.OnSimulationCommand(ResumeOrPauseSimulation, -1, -1);
                        end;
                        VK_ESCAPE:
                        begin
                            g_isRunning := False;
                        end;

                        VK_UP:
                        begin
                            GameOfLife.OnSimulationCommand(SpeedUpSimulation, -1, -1);
                        end;
                        VK_DOWN:
                        begin
                            GameOfLife.OnSimulationCommand(SlowDownSimulation, -1, -1);
                        end;

                    end;

                end;
            end;
            else
                Result := DefWindowProc(hWnd, Msg, wParam, lParam);
        end;
    end;

    procedure CreateWindow(const Width, Height: i32);
    var
        centerRect: RECT;
        style: DWORD;
    begin
        g_Window.Instance := GetModuleHandle(nil);
        g_Window.WindowClass.style := CS_HREDRAW or CS_VREDRAW or CS_OWNDC;
        g_Window.WindowClass.lpfnWndProc := @WindowProc;
        g_Window.WindowClass.cbClsExtra := 0;
        g_Window.WindowClass.cbWndExtra := 0;
        g_Window.WindowClass.hInstance := g_Window.Instance;
        g_Window.WindowClass.hIcon := LoadIcon(0, IDI_APPLICATION);
        g_Window.WindowClass.hCursor := LoadCursor(0, IDC_ARROW);
        g_Window.WindowClass.lpszMenuName := nil;
        g_Window.WindowClass.lpszClassName := 'MyWindowClass';
        RegisterClass(g_Window.WindowClass);

        // Configure style
        style := WS_OVERLAPPED or WS_CAPTION or WS_SYSMENU or WS_MINIMIZEBOX;

        // Center window
        centerRect.left := GetSystemMetrics(SM_CXSCREEN) div 2 - Width div 2;
        centerRect.top := GetSystemMetrics(SM_CYSCREEN) div 2 - Height div 2;
        centerRect.right := centerRect.left + Width;
        centerRect.bottom := centerRect.top + Height;

        g_Window.Handle := CreateWindowEx(
            0,
            g_Window.WindowClass.lpszClassName,
            'Game of Life',
            style,
            centerRect.left,
            centerRect.top,
            Width,
            Height,
            0,
            0,
            hInstance,
            nil);
    end;

    procedure Init();
    begin
        // Setup platform utils
        Windows.QueryPerformanceFrequency(@g_Platform.Frequency);
        Windows.QueryPerformanceFrequency(@g_Platform.Offset);

        // Creates a native window
        CreateWindow(1600, 900);

        // First resize and display
        ShowWindow(g_Window.Handle, SW_SHOW);

        // Initialize game
        GameOfLife.Initialialize();

        DebugLog.WriteLine('Successfully initialized.');
    end;

    procedure Run();
    var
        msg: Windows.MSG;
        buffer: OffscreenBuffer;

        clientArea: RECT;
        clientWidth, clientHeight: i32;
        deviceContext: HDC;
        timeStep, currentFrameTime, lastFrameTime: f32;
    begin
        timeStep := 0.0;
        lastFrameTime := 0.0;
        deviceContext := GetDC(g_Window.Handle);

        while (g_IsRunning) do
        begin
            // Listen to message queue
            while PeekMessage(msg, 0, 0, 0, PM_REMOVE) do
            begin
                if msg.message = WM_QUIT then
                begin
                    g_IsRunning := False;
                end;
                TranslateMessage(msg);
                DispatchMessage(msg);
            end;

            // Setup offscreen buffer
            buffer.Data := u32_ptr(g_OffscreenBuffer.Bitmap);
            buffer.Size := g_OffscreenBuffer.BitmapSize div sizeOf(u32); // Since we are casting it to u32 pointers
            buffer.Width := g_Window.Width;
            buffer.Height := g_Window.Height;

            // Pass it to the game layer
            GameOfLife.UpdateAndRender(buffer, timeStep);

            // Display buffer with client area dimenions
            GetClientRect(g_Window.Handle, clientArea);
            clientWidth := clientArea.right - clientArea.left;
            clientHeight := clientArea.bottom - clientArea.top;
            DisplayOffscreenBuffer(deviceContext, 0, 0, clientWidth, clientHeight);

            // Calculate timestep
            currentFrameTime := GetTimeStep();
            timeStep := currentFrameTime - lastFrameTime;
            lastFrameTime := currentFrameTime;
        end;
    end;

    procedure Shutdown();
    begin
        // OS will clean up
        DebugLog.WriteLine('Successfully shutdown.');
    end;

    // EntryPoint
begin
    Init();
    Run();
    Shutdown();
end.
